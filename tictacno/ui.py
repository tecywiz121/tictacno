from .events import Event, EventKind, ChooseEvent
from abc import ABCMeta, abstractmethod


class UI:
    '''
    User interface for the game.
    '''
    def __init__(self):
        self.screen = StartScreen()

    def step(self):
        print("Enter command: ")
        text = self.screen.instructions()
        character = input(text + '\n\n> ')
        (next_screen, event) = self.screen.handle_input(character)

        if next_screen is not None:
            self.screen = next_screen

        return event

    def render_board(self, board):
        text = ''

        if board is not None:
            text += '  '

            for (idx, spot) in enumerate(board):
                if spot is None:
                    text += '?'
                else:
                    text += spot

                if (idx + 1) % 3 == 0:
                    if idx != 8:
                        text += '\n ---+---+---\n  '
                else:
                    text += (' | ')

            text += '\n'

        return text


class Screen(metaclass=ABCMeta):
    @abstractmethod
    def handle_input(self, character):
        pass

    @abstractmethod
    def instructions(self):
        return ''


class StartScreen(Screen):
    def instructions(self):
        return '\n'.join([
            '(1) New Game',
            '(2) Load Game',
            '(3) Quit',
        ])

    def handle_input(self, character):
        if character == '1':
            return (GameScreen(), Event(EventKind.NEW_GAME))
        elif character == '2':
            return (GameScreen(), Event(EventKind.LOAD_GAME))
        elif character == '3':
            return (None, Event(EventKind.QUIT))
        else:
            return (None, None)


class GameScreen(Screen):
    def instructions(self):
        return '\n'.join([
            '',
            '  0 | 1 | 2 ',
            ' ---+---+---',
            '  3 | 4 | 5 ',
            ' ---+---+---',
            '  6 | 7 | 8 ',
        ])

    def handle_input(self, character):
        try:
            position = int(character)
        except ValueError:
            return (None, None)

        if position < 0 or position > 8:
            return (None, None)

        col = position // 3
        row = position % 3

        return (None, ChooseEvent(row, col))
