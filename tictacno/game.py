import random


class Game:
    def __init__(self, initial=None):
        if initial is None:
            initial = [None] * 9

        self.board = initial

    def choose(self, row, col):
        if row > 2 or col > 2:
            return False

        index = (row * 3) + col

        if self.board[index] is not None:
            return False

        self.board[index] = 'X'

        ai = self.play_opponent()
        self.board[ai] = 'O'

        return True

    def play_opponent(self):
        available = [
            idx for (idx, spot) in enumerate(self.board) if spot is None
        ]
        return random.choice(available)
