from .ui import UI
from .events import EventKind
from .game import Game


SAVE_FILE = 'tictacno.txt'


class TicTacNo:
    def __init__(self):
        self.game = None

    def handle_event(self, event):
        cont = True

        if event is None:
            cont = True

        elif event.kind == EventKind.QUIT:
            cont = False

        elif event.kind == EventKind.NEW_GAME:
            self.game = Game()

        elif event.kind == EventKind.LOAD_GAME:
            self.game = Game(self.load())

        elif event.kind == EventKind.CHOOSE:
            self.game.choose(event.row, event.col)

        if self.game is not None:
            self.save()

        return cont

    def load(self):
        '''
        Opens a file and calls `deserialize` on its contents.
        '''
        with open(SAVE_FILE, 'r') as f:
            return self.deserialize(f)

    def save(self):
        '''
        Calls `serialize` on a board, and writes it to a file.
        '''
        with open(SAVE_FILE, 'w') as f:
            f.write(self.serialize(self.game.board))

    def serialize(self, board):
        '''
        Converts a board into a string which can be parsed by `deserialize`.
        '''
        output = ''
        for (idx, spot) in enumerate(board):
            if spot is None:
                output += '?'
            else:
                output += spot

            if (idx + 1) % 3 == 0:
                output += '\n'
            else:
                output += ' '
        return output

    def deserialize(self, f):
        '''
        Converts a list or opened file into a board.
        '''
        loaded = []

        for line in f:
            for c in line.split(' '):
                c = c.strip()
                if c in ['X', 'O']:
                    loaded.append(c)
                else:
                    loaded.append(None)

        if len(loaded) != 9:
            raise 'Invalid Save'

        return loaded


def main():
    ui = UI()
    state = TicTacNo()

    while state.handle_event(ui.step()):
        if state.game is not None:
            text = ui.render_board(state.game.board)
            print(text)
