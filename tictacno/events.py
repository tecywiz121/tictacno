from enum import Enum, unique, auto


@unique
class EventKind(Enum):
    QUIT = auto()
    NEW_GAME = auto()
    LOAD_GAME = auto()
    CHOOSE = auto()


class Event:
    def __init__(self, kind):
        self.kind = kind


class ChooseEvent(Event):
    def __init__(self, row, col):
        super(ChooseEvent, self).__init__(EventKind.CHOOSE)
        self.row = row
        self.col = col
