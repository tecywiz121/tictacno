from tictacno.game import Game


def test_choose_occupied():
    board = [None] * 9
    board[0] = 'X'

    game = Game(board)
    game.choose(0, 0)

    assert game.board == ['X', None, None, None, None, None, None, None, None]
